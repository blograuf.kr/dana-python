from appium import webdriver
from appium.webdriver.common.touch_action import TouchAction
from appium.webdriver.common.appiumby import AppiumBy
from appium.webdriver.common.mobileby import MobileBy
from selenium.webdriver.support.wait import WebDriverWait
import pandas as pd
import time
import numpy as np
from datetime import date


# Driver Setup
caps = {
    "platformName": "Android",
    "appium:automationName": "UiAutomator2",
    "appium:deviceName": "d24b3b4c0421",
    "appium:ensureWebviewsHavePages": True,
    "appium:nativeWebScreenshot": True,
    "appium:newCommandTimeout": 3600,
    "appium:connectHardwareKeyboard": True
}

driver = webdriver.Remote("http://localhost:4723/wd/hub", caps)
wait = WebDriverWait(driver, 10, poll_frequency=1)
Tap_Action = TouchAction(driver)

deviceSize = driver.get_window_size()
print("Device Width and Height : ",deviceSize)
width = deviceSize['width']
height = deviceSize['height']
tanggal_hari_ini = str(date.today()).split('-')[2]
print('tanggal hari ini ',tanggal_hari_ini)

scrap_data = pd.read_csv('hasil_scraping.csv', encoding='utf-8')
scrap_data.jam_tanggal = pd.to_datetime(scrap_data.jam_tanggal)
scrap_data.sort_values('jam_tanggal', inplace=True)

def relogin():
    print('relogin')
    # Login No HP
    #driver.implicitly_wait(10)
    ele = wait.until(lambda  x: x.find_element(
        AppiumBy.XPATH,
        '//android.widget.EditText[@content-desc="txt-area-phone-number"]')
    )
    ele.send_keys('82182568412')

    ele = wait.until(lambda  x: x.find_element(
        AppiumBy.ID,
        'id.dana:id/btn_continue')
    )
    ele.click()

    # Pin
    #driver.implicitly_wait(10)
    ele = wait.until(lambda  x: x.find_element(
        AppiumBy.XPATH,
        '//android.widget.EditText[@content-desc="txt-area-pin"]')
    )
    ele.send_keys('591998')

    #time.sleep(5)

# Open Riwayat
def open_riwayat():
    print('open_riwayat')
    #driver.implicitly_wait(10)
    ele = wait.until(lambda  x: x.find_element(
            AppiumBy.XPATH,
            '(//android.widget.ImageView[@content-desc="navigation_tab_icon"])[6]')
    )
    Tap_Action.tap(ele).perform()

    # Urutkan dari terlama hingga terbaru
    # Menu Urutkan
    
    '''ele = wait.until(lambda  x: x.find_element(
                    AppiumBy.XPATH,
                    '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[3]/android.widget.Button[1]'
                    )
                )
                ele.click()
            
                # Point Terlama
                #driver.implicitly_wait(10)
                ele = wait.until(lambda  x: x.find_element(
                        AppiumBy.XPATH,
                        '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[4]/android.view.View/android.view.View[1]/android.view.View/android.view.View[2]/android.view.View/android.widget.TextView[2]'
                    )
                )
                Tap_Action.tap(ele).perform()
            
                # Tampilkan
                #driver.implicitly_wait(10)
                ele = wait.until(lambda  x: x.find_element(
                        AppiumBy.XPATH,
                        '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[4]/android.view.View/android.view.View[2]/android.widget.Button[2]'
                    )
                )
                ele.click()'''

    # pilih hari
    ele = wait.until(lambda  x: x.find_element(
        AppiumBy.XPATH,
        '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[3]/android.widget.Button[2]'
        )
    )
    ele.click()

    ele = wait.until(lambda  x: x.find_element(
        AppiumBy.XPATH,
        '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[4]/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]/android.widget.Button[1]'
        )
    )
    ele.click()

    if int(tanggal_hari_ini) < 15:
        for i in range(3,9):
            for j in range(1,8):
                ele = driver.find_element(
                        AppiumBy.XPATH,
                        f'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[4]/android.view.View/android.view.View[1]/android.view.View/android.widget.GridView/android.view.View[{i}]/android.view.View[{j}]'
                    )
                if ele.get_attribute('text') == tanggal_hari_ini:
                    break
            if ele.get_attribute('text') == tanggal_hari_ini:
                break
    else:
        for i in range(8,2,-1):
            for j in range(7,0,-1):
                ele = driver.find_element(
                        AppiumBy.XPATH,
                        f'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[4]/android.view.View/android.view.View[1]/android.view.View/android.widget.GridView/android.view.View[{i}]/android.view.View[{j}]'
                    )
                if ele.get_attribute('text') == tanggal_hari_ini:
                    break
            if ele.get_attribute('text') == tanggal_hari_ini:
                break
    ele.click()
    time.sleep(0.5)
    ele.click()

    ele = wait.until(lambda  x: x.find_element(
        AppiumBy.XPATH,
        '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[4]/android.view.View/android.view.View[2]/android.widget.Button[2]'
        )
    )
    ele.click()
    time.sleep(0.5)
    ele.click()
    time.sleep(1)
    Tap_Action.long_press(None, width/2, height*0.37).move_to(None, width/2, height*0.142).release().perform()


def do_api_limit():
    ele = wait.until(lambda  x: x.find_element(
            AppiumBy.XPATH,
            '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.widget.Button[2]'
        )
    )
    ele.click()

    #driver.implicitly_wait(10)
    ele = wait.until(lambda  x: x.find_element(
            AppiumBy.XPATH,
            '(//android.widget.ImageView[@content-desc="navigation_tab_icon"])[8]'
        )
    )
    ele.click()

    print('got it check')
    try:
        #driver.implicitly_wait(10)
        ele = wait.until(lambda  x: x.find_element(
                AppiumBy.XPATH,
                '//android.widget.Button[@content-desc="btnGotIt"]'
            )
        )
        ele.click()
    except:
        pass

    print('got it passed')
    time.sleep(1)
    Tap_Action.long_press(None, width/2, height*0.916).move_to(None, width/2, height*0.142).release().perform()
    Tap_Action.long_press(None, width/2, height*0.916).move_to(None, width/2, height*0.142).release().perform()

    #driver.implicitly_wait(10)
    ele = wait.until(lambda  x: x.find_element(
            AppiumBy.XPATH,
            '//android.widget.FrameLayout[@content-desc="btn-logout"]'
        )
    )
    ele.click()

    #driver.implicitly_wait(10)
    ele = wait.until(lambda  x: x.find_element(
            AppiumBy.XPATH,
            '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.Button[2]'
        )
    )
    ele.click()

    #driver.implicitly_wait(10)
    
    relogin()

    open_riwayat()

def cek_api_limit():
    try:
        time.sleep(1)
        if driver.find_element(AppiumBy.XPATH,'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.widget.TextView[5]').get_attribute('text') == 'exceed api limit' :
            return True
        else:
            return False
    except:
        return False

def get_data_inside():
    if not cek_api_limit():
        ele = wait.until(lambda  x: x.find_element(
                AppiumBy.XPATH,
                f'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[1]/android.view.View[1]/android.view.View[2]/android.widget.TextView[1]'
            )
        )
        
        jam_tanggal = ele.get_attribute('text')
        jam_tanggal = fix_tanggal(jam_tanggal)
        jam_tanggal = replace_month_with_number(jam_tanggal)

        jam_tanggal = pd.to_datetime(jam_tanggal, format="%d %m %Y %H:%M")

        Tap_Action.long_press(None, width/2, height*0.37).move_to(None, width/2, height*0.15).release().perform()

        for idx in np.arange(2,23):#[3,4,5,8,9,10,19,21]:
            try:
                ids = driver.find_element(
                                AppiumBy.XPATH,
                                f'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[1]/android.view.View[2]/android.widget.TextView[{idx}]'
                    ).get_attribute('text')

                if ids[0:2] == '20':
                    break
            except:
                pass
            '''
            loct = f'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[1]/android.view.View[2]/'
            loct = f'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[1]/android.view.View[2]/android.view.View[3]/android.widget.TextView'
            if driver.find_element(AppiumBy.XPATH,loct + f'android.widget.TextView[{idx}]').get_attribute('text') == 'ID Transaksi':
                ids = driver.find_element(
                                AppiumBy.XPATH,
                                loct + f'android.widget.TextView[{idx+1}]'
                    ).get_attribute('text')
                break
            '''
        if ids in scrap_data.id_transaksi.values:
            ele = wait.until(lambda  x: x.find_element(
                AppiumBy.ID,
                'id.dana:id/h5_tv_nav_back'
                )
            )
            ele.click()
            return 0, 0, 0, 0

        ele = wait.until(lambda  x: x.find_element(
                    AppiumBy.XPATH,
                    '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[1]/android.view.View[2]/android.widget.TextView[2]'
            )
        )
        nama = ele.get_attribute('text')
        try:
            nama = nama.split(' dari ')[1]
        except:
            nama = ''
        
        #driver.implicitly_wait(10)
        ele = wait.until(lambda  x: x.find_element(
                AppiumBy.XPATH,
                '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[1]/android.view.View[2]/android.widget.TextView[1]'
            )
        )
        status = ele.get_attribute('text')
        
        #driver.implicitly_wait(10)
        ele = wait.until(lambda  x: x.find_element(
                AppiumBy.ID,
                'id.dana:id/h5_tv_nav_back'
            )
        )
        ele.click()

        return jam_tanggal, ids, nama, status
    else:
        do_api_limit()


def fix_tanggal(text):
    text = text.replace(' • ',' ')
    return text

month_names = {
    "Jan": 1,
    "Feb": 2,
    "Mar": 3,
    "Apr": 4,
    "Mei": 5,
    "Jun": 6,
    "Jul": 7,
    "Agu": 8,
    "Sep": 9,
    "Okt": 10,
    "Nov": 11,
    "Des": 12
}

def replace_month_with_number(date_string):
    for month_abbr, month_num in month_names.items():
        date_string = date_string.replace(month_abbr, str(month_num))
    return date_string

# Open Dana Apk
#driver.implicitly_wait(10)
'''
ele = wait.until(lambda  x: x.find_element(
        AppiumBy.XPATH,
        '//android.widget.ImageView[@content-desc="DANA"]'
    )
)
Tap_Action.tap(ele).perform()
'''

#driver.implicitly_wait(10)
open_riwayat()

first_ids = -1
reach_last_refresh = 0
# Start Scraping
while True:
    if tanggal_hari_ini != str(date.today()).split('-')[2]:
        reach_last_refresh = 0
        ele = wait.until(lambda  x: x.find_element(
            AppiumBy.ID,
            'id.dana:id/h5_tv_nav_back'
            )
        )
        ele.click()
        open_riwayat()  
    if cek_api_limit():
        do_api_limit()
    try:
        if driver.find_element(AppiumBy.XPATH,'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[2]/android.widget.TextView[1]').get_attribute('text') == 'Hasil filter tidak ditemukan':
            Tap_Action.long_press(None, width/2, height*0.2).move_to(None, width/2, height*0.37).release().perform()
            time.sleep(3)
    except:
        pass
    #print('masuk')
    if reach_last_refresh >= 3:
        reach_last_refresh = 0
        ele = wait.until(lambda  x: x.find_element(
            AppiumBy.ID,
            'id.dana:id/h5_tv_nav_back'
            )
        )
        ele.click()
        open_riwayat()  
    for i in range(2,9):
        start_time = time.time()
        ele = wait.until(lambda  x: x.find_element(
                    AppiumBy.XPATH,
                    f'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[2]/android.view.View[{i}]/android.widget.TextView[3]'
                    #f'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[2]/android.view.View[2]/android.widget.TextView[3]'
                    #f'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[2]/android.view.View[2]/android.widget.TextView[3]'
                )
            )
        nominal = ele.get_attribute('text')

        ele = wait.until(lambda  x: x.find_element(
                AppiumBy.XPATH,
                f'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[2]/android.view.View[{i}]/android.widget.TextView[3]'
            )
        )
        ele.click()
        try:
            if driver.find_element(AppiumBy.XPATH,f'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[2]/android.view.View[{i}]/android.widget.TextView[3]').get_attribute('text') == nominal:
                pass
        except:
            jam_tanggal, ids, nama, status = get_data_inside()
            if i == 2:
                if first_ids != ids:
                    first_ids = ids
                else:
                    print(first_ids,ids)
                    ele = wait.until(lambda  x: x.find_element(
                        AppiumBy.ID,
                        'id.dana:id/h5_tv_nav_back'
                        )
                    )
                    ele.click()
                    open_riwayat()
            if [jam_tanggal, ids, nama, status] != [0,0,0,0]:
                print('time ',round(time.time()-start_time,2),'|',jam_tanggal, ids, nama, nominal, status)
                new_data = pd.DataFrame(
                    [[jam_tanggal, str(ids), str(nama), str(nominal), str(status)]],
                    columns=['jam_tanggal', 'id_transaksi', 'nama', 'nominal', 'status']
                )

                scrap_data = pd.concat([scrap_data, new_data], axis = 0)
                scrap_data.jam_tanggal = pd.to_datetime(scrap_data.jam_tanggal)
                scrap_data.sort_values('jam_tanggal', inplace=True)
                scrap_data.reset_index(drop=True,inplace=True)
                scrap_data.to_csv('hasil_scraping.csv', index=None, encoding='utf-8')
            else:
                reach_last_refresh += 1

    time.sleep(0.5)
    Tap_Action.long_press(None, width/2, height*0.81).move_to(None, width/2, height*0.155).release().perform()
    time.sleep(1)
    





driver.quit()





'''
# Start Scraping
Tap_Action.long_press(None, 500, 580).move_to(None, 500, 300).release().perform()
total_data = []
for z in range(10):
    # mengambil data 7 tab tertampil:
    # data jam tanggal
    for i in range(1, 10, 1):
        try:
            jam_tanggal = driver.find_element(
                AppiumBy.XPATH,
                f'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[2]/android.view.View[{i}]/android.widget.TextView[2]'
            ).get_attribute('text')

            transaksi = driver.find_element(
                AppiumBy.XPATH,
                f'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[2]/android.view.View[{i}]/android.widget.TextView[1]'
            ).get_attribute('text')

            nominal = driver.find_element(
                AppiumBy.XPATH,
                f'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[2]/android.view.View[{i}]/android.widget.TextView[3]'
            ).get_attribute('text')

            total_data.append([jam_tanggal, transaksi, nominal])
            print([jam_tanggal, transaksi, nominal])
        except:
            break

    Tap_Action.long_press(None, 500, 1951).move_to(None, 500, 300).release().perform()
    time.sleep(1)

print(
    pd.DataFrame(total_data, columns=[['Jam Tanggal', 'Asal Transaksi', 'Nominal']])
)
# def scrap_data(driver, df):
# Swipe Performe
# actions.long_press(None,startx,starty).move_to(None,endx,endy).release().perform()
'''

