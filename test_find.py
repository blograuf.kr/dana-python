from appium import webdriver
from appium.webdriver.common.touch_action import TouchAction
from appium.webdriver.common.appiumby import AppiumBy
from appium.webdriver.common.mobileby import MobileBy
from selenium.webdriver.support.wait import WebDriverWait
import pandas as pd
import time
import numpy as np
from datetime import date
import os

# Driver Setup
caps = {
    "platformName": "Android",
    "appium:automationName": "UiAutomator2",
    "appium:deviceName": "d24b3b4c0421",
    "appium:ensureWebviewsHavePages": True,
    "appium:nativeWebScreenshot": True,
    "appium:newCommandTimeout": 3600,
    "appium:connectHardwareKeyboard": True
}

#os.system("start /B start cmd.exe @cmd /k appium -a localhost -p 4723 --relaxed-security --allow-cors")
driver = webdriver.Remote("http://localhost:4723", caps)
wait = WebDriverWait(driver, 10, poll_frequency=1)
Tap_Action = TouchAction(driver)

deviceSize = driver.get_window_size()
print("Device Width and Height : ",deviceSize)
width = deviceSize['width']
height = deviceSize['height']
tanggal_hari_ini = str(date.today()).split('-')[2]
print('tanggal hari ini ',tanggal_hari_ini)

scrap_data = pd.read_csv('hasil_scraping.csv', encoding='utf-8')
scrap_data.jam_tanggal = pd.to_datetime(scrap_data.jam_tanggal)
scrap_data.sort_values('jam_tanggal', inplace=True)

def fix_tanggal(text):
    text = text.replace(' • ',' ')
    return text

month_names = {
    "Jan": 1,
    "Feb": 2,
    "Mar": 3,
    "Apr": 4,
    "Mei": 5,
    "Jun": 6,
    "Jul": 7,
    "Agu": 8,
    "Sep": 9,
    "Okt": 10,
    "Nov": 11,
    "Des": 12
}

def replace_month_with_number(date_string):
    for month_abbr, month_num in month_names.items():
        date_string = date_string.replace(month_abbr, str(month_num))
    return date_string

start_time = time.time()
a = driver.find_elements(
            AppiumBy.XPATH,
            f"//android.widget.TextView"
)


for ele in a:
	print(ele)
	print(ele.get_attribute('text'))
	print(ele.get_attribute('bounds'))
	if 'Rp' in ele.get_attribute('text'):# and ele.get_attribute('text').split('Rp')[0] != '-':
		#print(ele.get_attribute('text'))
		coor = ele.get_attribute('bounds')
		#print(coor)
		coor = coor[1:-1].split('][')[1].split(',')

		#Tap_Action.tap(None,int(coor[0])-10,int(coor[1])-10,1).perform()
		Tap_Action.tap(None,int(coor[0])-10,int(coor[1])-10,1).perform()
	
#print(ele_terpilih[0].get_attribute('bounds'))
#print(ele_terpilih[0].get_attribute('clickable'))
#data = np.array([b.get_attribute('text') for b in a[1:-1]])
#jam_tanggal = data[1]
#jam_tanggal = fix_tanggal(jam_tanggal)
#jam_tanggal = replace_month_with_number(jam_tanggal)

#jam_tanggal = pd.to_datetime(jam_tanggal, format="%d %m %Y %H:%M")

#ids = data[np.argwhere(data =='ID Transaksi')[0][0]+1]
#try:
#    nama = data[3].split(' dari ')[1]
#except:
#    nama = ''
#status = data[3]
#print(jam_tanggal, ids, nama, status)

print('time ',round(time.time()-start_time,2))