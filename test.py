'''import numpy as np

a = ['Transaksi',
	'15 Nov 2022 • 23:41',
	'DANA ID 0821••••8412',
	'Transaksi berhasil!',
	'Pembayaran ke Sky emperor',
	'PEMBAYARAN', 'Total Bayar',
	'Rp61.000', 'Metode Pembayaran',
	'Saldo DANA', 'Detail Transaksi',
	'Nama Acquirer', 'OVO', 'Nama Merchant',
	'Sky emperor Bandung ID', 'Merchant Location',
	'Bandung, 40264, ID', 'Merchant PAN',
	'9360091200003978157', 'ID Terminal',
	'Rwto0tT7rtiGWnsP', 'Customer PAN',
	'9360091533868075523', 'Reff ID',
	'007566930467', 'PAN Bisnis',
	'9360091200003978157', 'Reff ID',
	'007566930467', 'ID Transaksi',
	'20221115111212800100166677287080950',
	'ID Order Merchant', '• • • 66cb']
a = np.array(a)

print(a[np.argwhere(a =='ID Transaksi')[0][0]+1])

coor = '[515,210][1037,258]'
coor = coor[1:-1].split('][')[1].split(',')
coor[0] = int(coor[0])
coor[1] = int(coor[1])
print(coor)
'''
'''
import numpy as np
data = {
  "id_transaksi": {
    "0": "2022081410121420010100166094551002273",
    "1": "2022092410121481030100166676455194899",
    "2": "2022100710121481030100166676456818022",
    "3": "2022111510121481030100166676462115770",
    "4": "2022111610121481030100166676462168374",
    "5": "2022112910121481030100166676464101924",
    "6": "2022113010121481030100166676464303549",
    "7": "2023010210121420010100166916369898925",
    "8": "2023010510121420010100166916370505030",
    "9": "2023010910121420010100166916371088023",
    "10": "2023011510121481030100166676471977081",
    "11": "2023012410121420010100166916373534190",
    "12": "2023020310121481030100166676475243308",
    "13": "2023022110121420010100166916378621670",
    "14": "2023022610121481030100166676479291048",
    "15": "2023022810121481030100166676479673400",
    "16": "2023030210121481030100166676480083777",
    "17": "2023031010121420010100166916381784269",
    "18": "2023031210121481030100166676482060875",
    "19": "2023031310121481030100166676482219556",
    "20": "2023031310121420010100166916382409557",
    "21": "2023032010121481030100166676483647831",
    "22": "20230320111212802600166677576032341",
    "23": "20230320111212802600166677376024805"
  },
  "jam_tanggal": {
    "0": "2022-08-14 20:14:00",
    "1": "2022-09-24 23:21:00",
    "2": "2022-10-07 17:56:00",
    "3": "2022-11-15 19:57:00",
    "4": "2022-11-16 05:43:00",
    "5": "2022-11-29 02:57:00",
    "6": "2022-11-30 15:34:00",
    "7": "2023-01-02 21:18:00",
    "8": "2023-01-05 22:09:00",
    "9": "2023-01-09 13:16:00",
    "10": "2023-01-15 07:27:00",
    "11": "2023-01-24 09:20:00",
    "12": "2023-02-03 10:19:00",
    "13": "2023-02-21 21:15:00",
    "14": "2023-02-26 08:05:00",
    "15": "2023-02-28 10:14:00",
    "16": "2023-03-02 11:53:00",
    "17": "2023-03-10 16:20:00",
    "18": "2023-03-12 18:48:00",
    "19": "2023-03-13 14:12:00",
    "20": "2023-03-13 22:34:00",
    "21": "2023-03-20 22:48:00",
    "22": "2023-03-20 22:48:00",
    "23": "2023-03-20 22:49:00"
  },
  "nama": {
    "0": np.nan,
    "1": np.nan,
    "2": np.nan,
    "3": np.nan,
    "4": np.nan,
    "5": np.nan,
    "6": np.nan,
    "7": np.nan,
    "8": np.nan,
    "9": np.nan,
    "10": np.nan,
    "11": np.nan,
    "12": np.nan,
    "13": np.nan,
    "14": np.nan,
    "15": np.nan,
    "16": np.nan,
    "17": np.nan,
    "18": np.nan,
    "19": np.nan,
    "20": np.nan,
    "21": np.nan,
    "22": np.nan,
    "23": np.nan
  },
  "nominal": {
    "0": "Rp100.000",
    "1": "Rp151.000",
    "2": "Rp50.000",
    "3": "Rp100.000",
    "4": "Rp21.000",
    "5": "Rp15.000",
    "6": "Rp60.000",
    "7": "Rp50.000",
    "8": "Rp100.000",
    "9": "Rp100.000",
    "10": "Rp50.000",
    "11": "Rp900.000",
    "12": "Rp20.000",
    "13": "Rp500.000",
    "14": "Rp100.000",
    "15": "Rp20.000",
    "16": "Rp299.000",
    "17": "Rp500.000",
    "18": "Rp404.000",
    "19": "Rp100.000",
    "20": "Rp100.000",
    "21": "Rp42.000",
    "22": "Rp41.000",
    "23": "Rp41.000"
  },
  "status": {
    "0": "Berhasil diterima",
    "1": "Transaksi berhasil!",
    "2": "Transaksi berhasil!",
    "3": "Transaksi berhasil!",
    "4": "Transaksi berhasil!",
    "5": "Transaksi berhasil!",
    "6": "Transaksi berhasil!",
    "7": "Berhasil diterima",
    "8": "Berhasil diterima",
    "9": "Berhasil diterima",
    "10": "Transaksi berhasil!",
    "11": "Berhasil diterima",
    "12": "Transaksi berhasil!",
    "13": "Berhasil diterima",
    "14": "Transaksi berhasil!",
    "15": "Transaksi berhasil!",
    "16": "Transaksi berhasil!",
    "17": "Berhasil diterima",
    "18": "Transaksi berhasil!",
    "19": "Transaksi berhasil!",
    "20": "Berhasil diterima",
    "21": "Transaksi berhasil!",
    "22": "Refund berhasil!",
    "23": "Refund berhasil!"
  }
}
'''
#print(data.get_attribute)

import numpy as np
data = ['Transaksi','04 Agu 2023 • 05:00','DANA ID 0821••••8412'
'Berhasil diterima','Terima transfer Rp1 dari ENDANG ESNINAENI'
'TERIMA UANG','Total Terima','Rp1','Detail Transaksi','ID Transaksi'
'2023080410121420010100166727113842247','ID Order Merchant','• • • cfe5'
'','','*Termasuk PPN','PT Espay Debit Indonesia Koe'
'NPWP: 73.210.332.0-613.000'
'Capital Place Lt. 18, Jl. Jend. Gatot Subroto Kav. 18, Kuningan Barat, Mampang Prapatan'
'Jakarta Selatan. DKI Jakarta - 12710','']
data = ['Transaksi','24 Sep 2022 • 23:21','DANA ID 0821••••8412',
 'Transaksi berhasil!','Isi Saldo','ISI SALDO','Total Bayar','Rp151.000',
 'Metode Pembayaran','Transfer bank (BRI)','Detail Transaksi',
 'ID Transaksi','2022092410121481030100166676455194899',
 'ID Order Merchant','• • • 5197','','*Termasuk PPN',
 'PT Espay Debit Indonesia Koe','NPWP: 73.210.332.0-613.000',
 'Capital Place Lt. 18, Jl. Jend. Gatot Subroto Kav. 18, Kuningan Barat, Mampang Prapatan',
 'Jakarta Selatan. DKI Jakarta - 12710','']

data = np.array(data)
print(data == 'Metode Pembayaran')
#print(data[[dat[0:15] == 'Isi Saldo' for dat in data]])
#print(np.argwhere(data[:][:15] == 'Terima transfer'))
print(
    data[np.where(data == 'Metode Pembayaran')[0][0]+1]#, axis=0)
)

a = [['03 8 2023 19:55', '-Rp10.099'], ['04 8 2023 05:00', 'Rp1'], ['04 8 2023 05:01', 'Rp2'], ['04 8 2023 05:07', 'Rp2'], ['04 8 2023 05:08', 'Rp1'], ['04 8 2023 05:09', 'Rp1']]
c = [['03 8 2023 19:55', '-Rp10.099'], ['04 8 2023 05:00', 'Rp1'], ['04 8 2023 05:07', 'Rp2'], ['04 8 2023 05:08', 'Rp1'], ['04 8 2023 05:08', 'Rp3'], ['04 8 2023 05:09', 'Rp1']]

print(a == c)

import os
import pandas as pd

checkpoint_data = pd.DataFrame([])
for files in os.listdir('logs'):
    check_data = pd.read_csv(f'logs/{files}', encoding='utf-8')
    check_data.jam_tanggal = pd.to_datetime(check_data.jam_tanggal)
    check_data.sort_values('jam_tanggal', ascending = False,inplace=True)
    check_data.reset_index(drop=True,inplace=True)
    checkpoint_data = pd.concat([checkpoint_data, check_data], axis = 0)

print(checkpoint_data.jam_tanggal.iloc[0])