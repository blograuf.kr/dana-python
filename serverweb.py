from flask import Flask, render_template, redirect
from flask import Flask,jsonify,request
import pandas as pd
from json import loads, dumps
from datetime import date
import os
app = Flask(__name__)

@app.route('/')
def index():
    scrap_data = pd.read_csv('hasil_scraping.csv', encoding='utf-8')
    scrap_data.jam_tanggal = pd.to_datetime(scrap_data.jam_tanggal)
    scrap_data.sort_values('jam_tanggal', ascending=False, inplace=True)
    scrap_data.jam_tanggal = scrap_data.jam_tanggal.astype('string')
    return render_template('index - Copy.html', data=scrap_data.values)

@app.route('/getdata', methods = ['GET'])
def getdata():
    if(request.method == 'GET'):
        scrap_data = pd.read_csv('hasil_scraping.csv', encoding='utf-8')
        scrap_data.jam_tanggal = pd.to_datetime(scrap_data.jam_tanggal)
        scrap_data.sort_values('jam_tanggal', ascending=False, inplace=True)
        scrap_data.jam_tanggal = scrap_data.jam_tanggal.astype('string')
        scrap_data = scrap_data.to_dict()
        
        return jsonify(scrap_data)

@app.route('/tutup_buku', methods = ['GET'])
def tutup_buku():
    if(request.method == 'GET'):
        scrap_data = pd.read_csv('hasil_scraping.csv', encoding='utf-8')
        scrap_data.jam_tanggal = pd.to_datetime(scrap_data.jam_tanggal)
        scrap_data.sort_values('jam_tanggal', inplace=True)
        scrap_data.reset_index(drop=True,inplace=True)
        if f'{date.today()}.csv' in os.listdir('logs'):
            logs = pd.read_csv(f'logs/{date.today()}.csv', encoding='utf-8')

            scrap_data = pd.concat([scrap_data, logs], axis = 0)
            scrap_data.jam_tanggal = pd.to_datetime(scrap_data.jam_tanggal)
            scrap_data.sort_values('jam_tanggal', ascending=False, inplace=True)
            scrap_data.reset_index(drop=True,inplace=True)
            
        scrap_data.to_csv(f'logs/{date.today()}.csv', index=None, encoding='utf-8')

        new_data =  pd.DataFrame([],columns =scrap_data.columns )
        new_data.to_csv('hasil_scraping.csv', index=None, encoding='utf-8')
        return render_template('index - Copy.html', data=new_data.values)

@app.route('/start_services', methods = ['GET'])
def start_services():
    os.system('start /B start cmd.exe @cmd /c appium -p 4773 --relaxed-security --allow-cors')
    os.system('start /B start cmd.exe @cmd /c python main_3.py')
    #index()
    #return render_template('index - Copy.html')
    return redirect('/')

@app.route('/stop_services', methods = ['GET'])
def stop_services():
    os.system('taskkill /F /IM node.exe')
    #os.system('python main_3.py')
    #index()
    #return render_template('index - Copy.html')
    return redirect('/')

if __name__ == '__main__':
    app.run(debug=True)