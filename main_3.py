from appium import webdriver
from appium.webdriver.common.touch_action import TouchAction
from appium.webdriver.common.appiumby import AppiumBy
from appium.webdriver.common.mobileby import MobileBy
from selenium.webdriver.support.wait import WebDriverWait
import pandas as pd
import time
import numpy as np
from datetime import date
import os

# Driver Setup
caps = {
    "platformName": "Android",
    "appium:automationName": "UiAutomator2",
    "appium:deviceName": "d24b3b4c0421",
    "appium:ensureWebviewsHavePages": True,
    "appium:nativeWebScreenshot": True,
    "appium:newCommandTimeout": 3600,
    "appium:connectHardwareKeyboard": True
}

#os.system("start /B start cmd.exe @cmd /k appium -a localhost -p 4723 --relaxed-security --allow-cors")
driver = webdriver.Remote("http://localhost:4773", caps)
wait = WebDriverWait(driver, 10, poll_frequency=1)
Tap_Action = TouchAction(driver)

deviceSize = driver.get_window_size()
print("Device Width and Height : ",deviceSize)
width = deviceSize['width']
height = deviceSize['height']
tanggal_hari_ini = str(date.today()).split('-')[2]
print('tanggal hari ini ',tanggal_hari_ini)

scrap_data = pd.read_csv('hasil_scraping.csv', encoding='utf-8')
scrap_data.jam_tanggal = pd.to_datetime(scrap_data.jam_tanggal)
scrap_data.sort_values('jam_tanggal', ascending=False, inplace=True)

def relogin():
    print('relogin')
    # Login No HP
    #driver.implicitly_wait(10)
    ele = wait.until(lambda  x: x.find_element(
        AppiumBy.XPATH,
        '//android.widget.EditText[@content-desc="txt-area-phone-number"]')
    )
    ele.send_keys('')

    ele = wait.until(lambda  x: x.find_element(
        AppiumBy.ID,
        'id.dana:id/btn_continue')
    )
    ele.click()

    # Pin
    #driver.implicitly_wait(10)
    ele = wait.until(lambda  x: x.find_element(
        AppiumBy.XPATH,
        '//android.widget.EditText[@content-desc="txt-area-pin"]')
    )
    ele.send_keys('')

    #time.sleep(5)

# Open Riwayat
def open_riwayat():
    print('open_riwayat')
    #driver.implicitly_wait(10)
    ele = wait.until(lambda  x: x.find_element(
            AppiumBy.XPATH,
            '(//android.widget.ImageView[@content-desc="navigation_tab_icon"])[6]')
    )
    Tap_Action.tap(ele).perform()

    # Urutkan dari terlama hingga terbaru
    # Menu Urutkan
    
    ele = wait.until(lambda  x: x.find_element(
        AppiumBy.XPATH,
        '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[3]/android.widget.Button[1]'
        )
    )
    ele.click()

    # Point Terlama
    #driver.implicitly_wait(10)
    ele = wait.until(lambda  x: x.find_element(
            AppiumBy.XPATH,
            '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[4]/android.view.View/android.view.View[1]/android.view.View/android.view.View[2]/android.view.View/android.widget.TextView[2]'
        )
    )
    Tap_Action.tap(ele).perform()

    # Tampilkan
    #driver.implicitly_wait(10)
    ele = wait.until(lambda  x: x.find_element(
            AppiumBy.XPATH,
            '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[4]/android.view.View/android.view.View[2]/android.widget.Button[2]'
        )
    )
    ele.click()
               
    # pilih hari
    ele = wait.until(lambda  x: x.find_element(
        AppiumBy.XPATH,
        '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[3]/android.widget.Button[2]'
        )
    )
    ele.click()

    ele = wait.until(lambda  x: x.find_element(
        AppiumBy.XPATH,
        '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[4]/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]/android.widget.Button[1]'
        )
    )
    ele.click()

    if int(tanggal_hari_ini) < 15:
        for i in range(3,9):
            for j in range(1,8):
                ele = driver.find_element(
                        AppiumBy.XPATH,
                        f'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[4]/android.view.View/android.view.View[1]/android.view.View/android.widget.GridView/android.view.View[{i}]/android.view.View[{j}]'
                    )
                if ele.get_attribute('text') == tanggal_hari_ini:
                    break
            if ele.get_attribute('text') == tanggal_hari_ini:
                break
    else:
        for i in range(8,2,-1):
            for j in range(7,0,-1):
                ele = driver.find_element(
                        AppiumBy.XPATH,
                        f'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[4]/android.view.View/android.view.View[1]/android.view.View/android.widget.GridView/android.view.View[{i}]/android.view.View[{j}]'
                    )
                if ele.get_attribute('text') == tanggal_hari_ini:
                    break
            if ele.get_attribute('text') == tanggal_hari_ini:
                break
    ele.click()
    #time.sleep(0.3)
    ele.click()

    ele = wait.until(lambda  x: x.find_element(
        AppiumBy.XPATH,
        '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[3]/android.widget.Button[2]'
        )
    )
    ele.click()

    ele = wait.until(lambda  x: x.find_element(
        AppiumBy.XPATH,
        '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[4]/android.view.View/android.view.View[2]/android.widget.Button[2]'
        )
    )
    ele.click()
    #time.sleep(0.3)
    ele.click()
    #time.sleep(0.3)
    
    #Tap_Action.long_press(None, width/2, height*0.37).move_to(None, width/2, height*0.142).release().perform()

def do_api_limit():
    ele = wait.until(lambda  x: x.find_element(
            AppiumBy.XPATH,
            '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.widget.Button[2]'
        )
    )
    ele.click()

    #driver.implicitly_wait(10)
    ele = wait.until(lambda  x: x.find_element(
            AppiumBy.XPATH,
            '(//android.widget.ImageView[@content-desc="navigation_tab_icon"])[8]'
        )
    )
    ele.click()

    print('got it check')
    try:
        #driver.implicitly_wait(10)
        ele = wait.until(lambda  x: x.find_element(
                AppiumBy.XPATH,
                '//android.widget.Button[@content-desc="btnGotIt"]'
            )
        )
        ele.click()
    except:
        pass

    print('got it passed')
    time.sleep(1)
    Tap_Action.long_press(None, width/2, height*0.916).move_to(None, width/2, height*0.142).release().perform()
    Tap_Action.long_press(None, width/2, height*0.916).move_to(None, width/2, height*0.142).release().perform()

    #driver.implicitly_wait(10)
    ele = wait.until(lambda  x: x.find_element(
            AppiumBy.XPATH,
            '//android.widget.FrameLayout[@content-desc="btn-logout"]'
        )
    )
    ele.click()

    #driver.implicitly_wait(10)
    ele = wait.until(lambda  x: x.find_element(
            AppiumBy.XPATH,
            '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.Button[2]'
        )
    )
    ele.click()

    #driver.implicitly_wait(10)
    
    relogin()

    open_riwayat()

def cek_api_limit():
    try:
        time.sleep(1)
        if driver.find_element(AppiumBy.XPATH,'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.widget.TextView[5]').get_attribute('text') == 'exceed api limit' :
            return True
        else:
            return False
    except:
        return False

def get_data_inside():
    print('get data inside',end=' ')
    ele = wait.until(lambda  x: x.find_element(
                    AppiumBy.XPATH,
                    '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[2]/android.widget.Button'
                )
            )
    print('start')
    #time.sleep(0.5)
    a = driver.find_elements(
                AppiumBy.XPATH,
                f"//android.widget.TextView"
            )

    data = np.array([b.get_attribute('text') for b in a[1:-1]])
    jam_tanggal = data[1]
    jam_tanggal = fix_tanggal(jam_tanggal)
    jam_tanggal = replace_month_with_number(jam_tanggal)

    jam_tanggal = pd.to_datetime(jam_tanggal, format="%d %m %Y %H:%M")
    
    try:
        nominal = data[np.where(data == 'Biaya Admin')[0][0]-1]
        fee = data[np.where(data == 'Biaya Admin')[0][0]+1]
    except:
        try:
            nominal = data[np.where(data == 'Total Terima')[0][0]+1]
            fee = 'Rp0'
        except:
            nominal = data[np.where(data == 'Total Bayar')[0][0]+1]
            fee = 'Rp0'


    ids = data[np.argwhere(data =='ID Transaksi')[0][0]+1]
    if ids in scrap_data.id_transaksi.values:
        ele = wait.until(lambda  x: x.find_element(
            AppiumBy.ID,
            'id.dana:id/h5_tv_nav_back'
            )
        )
        ele.click()
        return 0, 0, 0, 0, 0, 0
    try:
        #print(nama)
        nama = data[[dat[0:15] == 'Terima transfer' for dat in data]][0]
        nama = nama.split(' dari ')[1]
        #print(nama)
    except:
        try:
            nama = data[np.where(data == 'Metode Pembayaran')[0][0]+1]
        except:
            nama = data[4]
    status = data[3]

    #driver.implicitly_wait(10)
    while True:
        try:
            ele = wait.until(lambda  x: x.find_element(
                    AppiumBy.ID,
                    'id.dana:id/h5_tv_nav_back'
                )
            )
            ele.click()
            ele = wait.until(lambda  x: x.find_element(
                AppiumBy.XPATH,
                f'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[1]/android.widget.ListView/android.view.View[1]'
            ))

            if ele.get_attribute('text') == "Berlangsung":
                break
        except:
            pass



    return jam_tanggal, ids, nama, nominal, fee, status
    #else:
    #    do_api_limit()

def fix_tanggal(text):
    text = text.replace(' • ',' ')
    return text

month_names = {
    "Jan": 1,
    "Feb": 2,
    "Mar": 3,
    "Apr": 4,
    "Mei": 5,
    "Jun": 6,
    "Jul": 7,
    "Agu": 8,
    "Sep": 9,
    "Okt": 10,
    "Nov": 11,
    "Des": 12
}

def replace_month_with_number(date_string):
    for month_abbr, month_num in month_names.items():
        date_string = date_string.replace(month_abbr, str(month_num))
    return date_string

def refresh_pages():
    print('do refresh')
    ele = wait.until(lambda  x: x.find_element(
        AppiumBy.XPATH,
        '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[3]/android.widget.Button[2]'
        )
    )
    ele.click()
    
    ele = wait.until(lambda  x: x.find_element(
        AppiumBy.XPATH,
        '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[4]/android.view.View/android.view.View[2]/android.widget.Button[2]'
        )
    )
    ele.click()
    time.sleep(0.1)
    ele.click()
    ele = wait.until(lambda  x: x.find_element(
                        AppiumBy.XPATH,
                        f'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[1]/android.widget.ListView/android.view.View[1]'
                    ).get_attribute('text'))
    '''
    ele = wait.until(lambda  x: x.find_element(
            AppiumBy.ID,
            'id.dana:id/h5_tv_nav_back'
        )
    )
    ele.click()

    open_riwayat()

    checkpoint_data = pd.read_csv('hasil_scraping.csv', encoding='utf-8')['jam_tanggal'].values
    time.sleep(0.3)
    Tap_Action.long_press(None, width/2, height*0.81).move_to(None, width/2, height*0.155).release().perform()
    #Tap_Action.long_press(None, width/2, height*0.71).move_to(None, width/2, height*0.2).release().perform()
    time.sleep(0.3)
    while True:
        for i in range(2,9):
            tgl = driver.find_element(
                            AppiumBy.XPATH,
                            f'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[2]/android.view.View[{i}]/android.widget.TextView[2]'
                        ).get_attribute('text')
            #print(tgl)
            tgl = fix_tanggal(tgl)
            tgl = replace_month_with_number(tgl)
            #print(tgl)
            if pd.to_datetime(np.unique(checkpoint_data)[-2]) > pd.to_datetime(tgl,dayfirst=True):
                time.sleep(0.3)
                Tap_Action.long_press(None, width/2, height*0.81).move_to(None, width/2, height*0.155).release().perform()
                #Tap_Action.long_press(None, width/2, height*0.71).move_to(None, width/2, height*0.2).release().perform()
                time.sleep(0.3)
            else:
                return 0
    '''

def Checkpoint_search():
    print('checkpoint search')
    #checkpoint_data = pd.read_csv('hasil_scraping.csv', encoding='utf-8')['jam_tanggal'].values
    scrap_data = pd.read_csv('hasil_scraping.csv', encoding='utf-8')
    scrap_data.jam_tanggal = pd.to_datetime(scrap_data.jam_tanggal)
    scrap_data.sort_values('jam_tanggal', ascending=False, inplace=True)
    checkpoint_data = pd.DataFrame([])
    for files in os.listdir('logs'):
        check_data = pd.read_csv(f'logs/{files}', encoding='utf-8')
        check_data.jam_tanggal = pd.to_datetime(check_data.jam_tanggal)
        check_data.sort_values('jam_tanggal', ascending = False,inplace=True)
        check_data.reset_index(drop=True,inplace=True)
        checkpoint_data = pd.concat([checkpoint_data, check_data], axis = 0)

    checkpoint_data = pd.concat([checkpoint_data, scrap_data], axis = 0)
    checkpoint_data.jam_tanggal = pd.to_datetime(checkpoint_data.jam_tanggal)
    checkpoint_data.sort_values('jam_tanggal', ascending = False,inplace=True)
    checkpoint_data.reset_index(drop=True,inplace=True)
    checkpoint_data = checkpoint_data['jam_tanggal'].values
    '''
    time.sleep(0.3)
    Tap_Action.long_press(None, width/2, height*0.81).move_to(None, width/2, height*0.155).release().perform()
    #Tap_Action.long_press(None, width/2, height*0.71).move_to(None, width/2, height*0.2).release().perform()
    time.sleep(0.3)
    '''
    while True:
        try:
            if wait.until(lambda  x: x.find_element(
                    AppiumBy.XPATH,
                    f'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[2]/android.widget.TextView[1]'
                    )).get_attribute('text') == "Hasil filter tidak ditemukan":
                ele = wait.until(lambda  x: x.find_element(
                    AppiumBy.XPATH,
                    '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[3]/android.widget.Button[2]'
                    )
                )
                ele.click()
                
                ele = wait.until(lambda  x: x.find_element(
                    AppiumBy.XPATH,
                    '/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[4]/android.view.View/android.view.View[2]/android.widget.Button[2]'
                    )
                )
                ele.click()
                time.sleep(3)
            else:
                for i in range(2,9):
                    try:
                        tgl = driver.find_element(
                                        AppiumBy.XPATH,
                                        f'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[2]/android.view.View[{i}]/android.widget.TextView[2]'
                                    ).get_attribute('text')
                        #print(tgl)
                        tgl = fix_tanggal(tgl)
                        tgl = replace_month_with_number(tgl)
                        #print(tgl)
                        if pd.to_datetime(np.unique(checkpoint_data)[-1]) > pd.to_datetime(tgl,dayfirst=True):
                            time.sleep(0.3)
                            Tap_Action.long_press(None, width/2, height*0.81).move_to(None, width/2, height*0.155).release().perform()
                            #Tap_Action.long_press(None, width/2, height*0.71).move_to(None, width/2, height*0.2).release().perform()
                            time.sleep(0.3)
                        else:
                            return 0
                    except:
                        pass
        except:
            pass
#open_riwayat()

# Start Scraping
first_ids = -1
reach_last_refresh = 0

trans = 0
trans_time = time.time()
trigger = False
last_checkpoint = []

#if scrap_data.shape[0] == 0:
Checkpoint_search()

while True:
    checkpoint = []
    for i in range(2,9):
        try:
            nominal = driver.find_element(
                        AppiumBy.XPATH,
                        f'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[2]/android.view.View[{i}]/android.widget.TextView[3]'
                    ).get_attribute('text')
            tgl = driver.find_element(
                        AppiumBy.XPATH,
                        f'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[2]/android.view.View[{i}]/android.widget.TextView[2]'
                    ).get_attribute('text')
            tgl = fix_tanggal(tgl)
            tgl = replace_month_with_number(tgl)
            checkpoint.append(tgl)
            checkpoint.append(nominal)
            if 'Rp' in nominal and nominal.split('Rp')[0] != '-':
                driver.find_element(
                        AppiumBy.XPATH,
                        f'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[2]/android.view.View[{i}]/android.widget.TextView[3]'
                    ).click()
                #try:
                #    if driver.find_element(AppiumBy.XPATH,f'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[2]/android.view.View[{i}]/android.widget.TextView[3]').get_attribute('text') == nominal:
                #        pass
                #except:
                start_time = time.time()   
                jam_tanggal, ids, nama, nominal,fee, status = get_data_inside()
                        
                if [jam_tanggal, ids, nama, nominal, fee, status] != [0,0,0,0,0,0]:
                    print(trans,'| ',round(time.time()-start_time,2),'detik |',jam_tanggal, ids, nama, nominal, status)
                    new_data = pd.DataFrame(
                        [[jam_tanggal, str(ids), str(nama), str(nominal), str(fee), str(status)]],
                        columns=['jam_tanggal', 'id_transaksi', 'nama', 'nominal', 'fee', 'status']
                    )

                    scrap_data = pd.concat([scrap_data, new_data], axis = 0)
                    scrap_data.jam_tanggal = pd.to_datetime(scrap_data.jam_tanggal)
                    scrap_data.sort_values('jam_tanggal', ascending=False, inplace=True)
                    scrap_data.reset_index(drop=True,inplace=True)
                    scrap_data.to_csv('hasil_scraping.csv', index=None, encoding='utf-8')
                    
                else:
                    #reach_last_refresh += 1
                    pass
        except:
            pass

        trans+=1
    time.sleep(0.3)
    Tap_Action.long_press(None, width/2, height*0.81).move_to(None, width/2, height*0.155).release().perform()
    #Tap_Action.long_press(None, width/2, height*0.71).move_to(None, width/2, height*0.2).release().perform()
    time.sleep(0.3)

    #print('-'*30)
    #print(last_checkpoint)
    #print(checkpoint)
    #print(last_checkpoint ==checkpoint)
    try :
        if wait.until(lambda  x: x.find_element(
                AppiumBy.XPATH,
                f'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[2]/android.widget.TextView'
                )).get_attribute('text') == "  Tidak ada pembaharuan data  ":
            refresh_pages()
        elif wait.until(lambda  x: x.find_element(
                AppiumBy.XPATH,
                f'/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout[2]/android.widget.RelativeLayout/android.widget.RelativeLayout/android.view.ViewGroup/android.widget.FrameLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View[2]/android.widget.TextView[4]'
                )).get_attribute('text') == "  Tidak ada pembaharuan data  ":
            refresh_pages()
        
    except:
        pass
    #if last_checkpoint == checkpoint:
    #    refresh_pages()
        #last_checkpoint = []
    #else:
    #    last_checkpoint = checkpoint
    #except:
    #    pass
    

print('total waktu 100 transaksi : ',round(time.time()-trans_time,2))
driver.quit()
